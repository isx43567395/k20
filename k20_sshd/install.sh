#! /bin/bash
# Kserver
# @edt ASIX M11-SAD Curs 2020-2021


#dnf install pam_krb5-2.4.8-6.el7.x86_64.rpm
# wget https://rpmfind.net/linux/centos/7.9.2009/os/x86_64/Packages/pam_krb5-2.4.8-6.el7.x86_64.rpm

cp /opt/docker/krb5.conf /etc/krb5.conf
bash /opt/docker/auth.sh
/usr/bin/ssh-keygen -A
cp /opt/docker/sshd_config /etc/ssh/sshd_config
kadmin -p admin -w kadmin -q "ktadd -k /etc/krb5.keytab host/sshd.edt.org"

#cp /opt/docker/ldap.conf /etc/openldap/ldap.conf
#cp /opt/docker/nsswitch.conf /etc/nsswitch.conf
#cp /opt/docker/nslcd.conf /etc/nslcd.conf
#cp /opt/docker/system-auth /etc/pam.d/system-auth

dnf -y install pam_krb5-2.4.8-6.el7.x86_64.rpm

#Crear usuaris local01..03 (IP+AP)
for user in local01 local02 local03
do 
  useradd $user
  echo -e "$user\n$user\n" | passwd --stdin $user
done

#Crear usuaris kuser01..03 (IP)
for user in kuser{01..06}
do
  useradd $user
done
