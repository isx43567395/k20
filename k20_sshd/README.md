#K20 SSHD SERVER

@isx43567395 ASIX M11-SAd Curs 2020 - 2021

### Autenticació


 * **zeuslawl/k20:sshd** servidor SSH amb PAM amb autenticació AP de kerberos i IP de ldap. El servidor kerberos al que contacta s'ha de dir kserver.edt.org. El servidor ldap s'anomena ldap.edt.org. Aquest host es configura amb authconfig. Generat a partir de la imatge zeuslawl/k20_khost-pam-ldap que és un host amb PAM kerb5+ldap. Se li ha afegit:

*  el servei sshd (cal generar les claus de host)
*  el keytab contenint el princpial host/sshd.edt.org@EDT.ORG
*  la configuració sshd_config per permetre connexions kerberitzades
*  la configuració ssh_config per pode fer test des del propi host.

En /etc/hosts ponemos ip del servidor de amazon:
A.B.C.D kserver.edt.org kserver

**SERVER:**
docker run --rm --name kserver.edt.org -h kserver.edt.org --net 2hisix -d zeuslawl/k20:kserver

**SERVER CON PROPAGACIÓN DE PUERTOS**
docker run --rm --name kserver.edt.org -h kserver.edt.org --net 2hisix -p 88:88 -p 464:464 -p 749:749 -d zeuslawl/k20:kserver

**CLIENTE:**
docker run --rm --name sshd.edt.org -h sshd.edt.org --net 2hisix -it zeuslawl/k20:sshd

**SERVIDOR SSH CON DETACH**
docker run --rm --name sshd.edt.org -h sshd.edt.org --net 2hisix -d zeuslawl/k20:sshd

**SERVIDOR SSH CON PROPAGACIÓN DE PUERTOS PARA AMI**
docker run --rm --name sshd.edt.org -h sshd.edt.org --net 2hisix -p 2022:22 -d zeuslawl/k20:sshd

Comentar el include del /etc/krb5.conf 

**INICIAR SERVIDOR LDAP**
docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisix -p 389:389 -d zeuslawl/ldap20:group
