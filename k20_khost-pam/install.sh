#! /bin/bash
# Kserver
# @edt ASIX M11-SAD Curs 2020-2021


#dnf install pam_krb5-2.4.8-6.el7.x86_64.rpm
# wget https://rpmfind.net/linux/centos/7.9.2009/os/x86_64/Packages/pam_krb5-2.4.8-6.el7.x86_64.rpm

cp /opt/docker/krb5.conf /etc/krb5.conf

dnf -y install pam_krb5-2.4.8-6.el7.x86_64.rpm

cp /opt/docker/system-auth /etc/pam.d/system-auth

#Crear usuaris local01..03 (IP+AP)
for user in local01 local02 local03
do 
  useradd $user
  echo -e "$user\n$user\n" | passwd --stdin $user
done

#Crear usuaris kuser01..03 (IP)
for user in kuser{01..06}
do
  useradd $user
done

