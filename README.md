# Kerberos
## @isx43567395 ASIX M11-SAD Curs 2020-2021

Podeu trobar les imatges docker al Dockehub de [zeuslawl](https://hub.docker.com/u/zeuslawl/)

Podeu trobar la documentació del mòdul a [ASIX-M11](https://sites.google.com/site/asixm11edt/) Docu del profe.

ASIX M11-SAD Escola del treball de barcelona

### Authenticació

**zeuslawl/k20:kserver** servidor kerberos detach. Crea els usuaris pere
  pau (admin), jordi, anna, marta, marta/admin i julia.
  Assignar-li el nom de host: *kserver.edt.org* (F32)

**zeuslawl/k20:khost** host client de kerberos. Simplement amb eines 
  kinit, klist i kdestroy (no pam). El servidor al que contacta s'ha 
  de dir *kserver.edt.org*. (F32)

**zeuslawl/k20:khost-pam** host amb PAM de kerberos. El servidor al que contacta s'ha de dir kserver.edt.org. Aquest host configura el system-auth de pam per usar el mòdul pam_krb5.so. (F32)

zeuslawl/k20:khost-pam-ldap (khost-pam-ldap) host amb PAM amb autenticació AP de kerberos i IP de ldap. El servidor kerberos al que contacta s'ha de dir kserver.edt.org. El servidor ldap s'anomena ldap.edt.org. Aquest host es configura amb authconfig (fet amb fedora 27)
