#K20 Kerveros host client

@isx43567395 ASIX M11-SAd Curs 2020 - 2021

### Autenticació


 * **zeuslawl/k20:khost** host client de kerberos. Simplement amb eines kinit, klist i 
   kdestroy (no pam). El servidor al que contacta s'ha de dir *kserver.edt.org*.

En /etc/hosts ponemos ip del servidor de amazon:
A.B.C.D kserver.edt.org kserver

**SERVER:**
docker run --rm --name kserver.edt.org -h kserver.edt.org --net 2hisix -d zeuslawl/k20:kserver

**SERVER CON PROPAGACIÓN DE PUERTOS**
docker run --rm --name kserver.edt.org -h kserver.edt.org --net 2hisix -p 88:88 -p 464:464 -p 749:749 -d zeuslawl/k20:kserver

**CLIENTE:**
docker run --rm --name khost.edt.org -h khost.edt.org --net 2hisix -it zeuslawl/k20:khost-pam-ldap

**CLIENTE DETACH**
docker run --rm --name khost.edt.org -h khost.edt.org --net 2hisix -d zeuslawl/k20:khost-pam-ldap

**INICIAR SERVIDOR LDAP**
docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisix -p 389:389 -d zeuslawl/ldap20:group
