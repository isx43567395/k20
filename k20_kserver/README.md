# K20 Kerveros server

@isx43567395 ASIX M11-SAd Curs 2020 - 2021

### Autenticació

 * **edtasixm11/k20:kserver** Servidor kerberos detach. Crea els usuaris pere pau (admin), 
   jordi, anna, marta, marta/admin i julia. Assignar-li el nom de host: kserver.edt.org

**SERVER:**
docker run --rm --name kserver.edt.org -h kserver.edt.org --net 2hisix -d zeuslawl/k20:kserver

**SERVER: con Propagación de puertos**
docker run --rm --name kserver.edt.org -h kserver.edt.org --net 2hisix -p 88:88 -p 464:464 -p 749:749 -d zeuslawl/k20:kserver

**CLIENTE:**
docker run --rm --name khost.edt.org -h khost.edt.org --net 2hisix -it zeuslawl/k20:khost /bin/bash 

